const mongoose = require("mongoose");

const connectDb = (url) => {
  mongoose.set("strictQuery", false);
  return mongoose.connect(url, {
    ssl: true,
    sslValidate: false,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
};

module.exports = connectDb;
