const User = require("../models/User");
const jwt = require("jsonwebtoken");
const { UnauthenticatedError } = require("../errors");

const auth = async (req, res, next) => {
  // check header

  const authHeader = req.headers.authorization || req.headers.Authorization;
  if (!authHeader || !authHeader.startsWith("Bearer ")) {
    throw new UnauthenticatedError("Authetication invalid");
  }
  const token = authHeader.split(" ")[1];
  try {
    const payload = jwt.verify(token, process.env.JWT_SECRET);
    // attach the user to the job routes

    req.user = { userId: payload.userId, name: payload.name };
    next();
  } catch (error) {
    console.error(error);
    throw new UnauthenticatedError("Authentication invalid");
  }
};

module.exports = auth;

// const jwt = require("jsonwebtoken");
// const { UnauthenticatedError } = require("../errors");

// const authenticationMiddleware = async (req, res, next) => {
//   const authHeader = req.headers.authorization || req.headers.Authorization;
//   if (!authHeader || !authHeader.startsWith("Bearer ")) {
//     throw new UnauthenticatedError("No token provided");
//   }
//   const token = authHeader.split(" ")[1];
//   try {
//     const decoded = jwt.verify(token, process.env.JWT_SECRET);
//     const { id, username } = decoded;
//     req.user = { id, username };
//     next();
//   } catch (error) {
//     throw new UnauthenticatedError("Not authorized to access this route");
//   }
// };

// module.exports = authenticationMiddleware;
